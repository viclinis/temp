﻿    using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class estadoObsersController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estadoObsers/

        public ActionResult Index()
        {
            return View(db.estado_obser.ToList());
        }

        //
        // GET: /estadoObsers/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // GET: /estadoObsers/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estadoObsers/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_obser estado_obser)
        {
            if (ModelState.IsValid)
            {
                db.estado_obser.Add(estado_obser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_obser);
        }

        //
        // GET: /estadoObsers/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // POST: /estadoObsers/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_obser estado_obser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_obser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_obser);
        }

        //
        // GET: /estadoObsers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // POST: /estadoObsers/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            db.estado_obser.Remove(estado_obser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}