﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class serviciospublicosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /serviciospublicos/

        public ActionResult Index()
        {
            var servicios_publicos = db.servicios_publicos.Include(s => s.estado_serpub);
            return View(servicios_publicos.ToList());
        }

        //
        // GET: /serviciospublicos/Details/5

        public ActionResult Details(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            return View(servicios_publicos);
        }

        //
        // GET: /serviciospublicos/Create

        public ActionResult Create()
        {
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub");
            return View();
        }

        //
        // POST: /serviciospublicos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(servicios_publicos servicios_publicos)
        {
            if (ModelState.IsValid)
            {
                db.servicios_publicos.Add(servicios_publicos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // GET: /serviciospublicos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // POST: /serviciospublicos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(servicios_publicos servicios_publicos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicios_publicos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // GET: /serviciospublicos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            return View(servicios_publicos);
        }

        //
        // POST: /serviciospublicos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            db.servicios_publicos.Remove(servicios_publicos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}