﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class vigenciasController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /vigencias/

        public ActionResult Index()
        {
            return View(db.vigencias.ToList());
        }

        //
        // GET: /vigencias/Details/5

        public ActionResult Details(int id = 0)
        {
            vigencia vigencia = db.vigencias.Find(id);
            if (vigencia == null)
            {
                return HttpNotFound();
            }
            return View(vigencia);
        }

        //
        // GET: /vigencias/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /vigencias/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vigencia vigencia)
        {
            if (ModelState.IsValid)
            {
                db.vigencias.Add(vigencia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vigencia);
        }

        //
        // GET: /vigencias/Edit/5

        public ActionResult Edit(int id = 0)
        {
            vigencia vigencia = db.vigencias.Find(id);
            if (vigencia == null)
            {
                return HttpNotFound();
            }
            return View(vigencia);
        }

        //
        // POST: /vigencias/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vigencia vigencia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vigencia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vigencia);
        }

        //
        // GET: /vigencias/Delete/5

        public ActionResult Delete(int id = 0)
        {
            vigencia vigencia = db.vigencias.Find(id);
            if (vigencia == null)
            {
                return HttpNotFound();
            }
            return View(vigencia);
        }

        //
        // POST: /vigencias/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            vigencia vigencia = db.vigencias.Find(id);
            db.vigencias.Remove(vigencia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}