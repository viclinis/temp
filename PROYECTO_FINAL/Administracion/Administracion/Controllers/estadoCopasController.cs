﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class estadoCopasController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estadoCopas/

        public ActionResult Index()
        {
            return View(db.estado_copa.ToList());
        }

        //
        // GET: /estadoCopas/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // GET: /estadoCopas/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estadoCopas/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_copa estado_copa)
        {
            if (ModelState.IsValid)
            {
                db.estado_copa.Add(estado_copa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_copa);
        }

        //
        // GET: /estadoCopas/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // POST: /estadoCopas/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_copa estado_copa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_copa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_copa);
        }

        //
        // GET: /estadoCopas/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // POST: /estadoCopas/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            db.estado_copa.Remove(estado_copa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}