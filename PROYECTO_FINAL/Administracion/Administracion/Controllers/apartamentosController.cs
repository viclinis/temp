﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class apartamentosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /apartamentos/

        public ActionResult Index()
        {
            var apartamentos = db.apartamentos.Include(a => a.estado_apto).Include(a => a.piso);
            return View(apartamentos.ToList());
        }

        //
        // GET: /apartamentos/Details/5

        public ActionResult Details(int id = 0)
        {
            apartamento apartamento = db.apartamentos.Find(id);
            if (apartamento == null)
            {
                return HttpNotFound();
            }
            return View(apartamento);
        }

        //
        // GET: /apartamentos/Create

        public ActionResult Create()
        {
            ViewBag.Id_EstadoApto = new SelectList(db.estado_apto, "Id_EstadoApto", "Id_EstadoApto");
            ViewBag.Num_piso = new SelectList(db.pisoes, "Num_piso", "Detalle");
            return View();
        }

        //
        // POST: /apartamentos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(apartamento apartamento)
        {
            if (ModelState.IsValid)
            {
                db.apartamentos.Add(apartamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_EstadoApto = new SelectList(db.estado_apto, "Id_EstadoApto", "Id_EstadoApto", apartamento.Id_EstadoApto);
            ViewBag.Num_piso = new SelectList(db.pisoes, "Num_piso", "Detalle", apartamento.Num_piso);
            return View(apartamento);
        }

        //
        // GET: /apartamentos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            apartamento apartamento = db.apartamentos.Find(id);
            if (apartamento == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_EstadoApto = new SelectList(db.estado_apto, "Id_EstadoApto", "Id_EstadoApto", apartamento.Id_EstadoApto);
            ViewBag.Num_piso = new SelectList(db.pisoes, "Num_piso", "Detalle", apartamento.Num_piso);
            return View(apartamento);
        }

        //
        // POST: /apartamentos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(apartamento apartamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apartamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_EstadoApto = new SelectList(db.estado_apto, "Id_EstadoApto", "Id_EstadoApto", apartamento.Id_EstadoApto);
            ViewBag.Num_piso = new SelectList(db.pisoes, "Num_piso", "Detalle", apartamento.Num_piso);
            return View(apartamento);
        }

        //
        // GET: /apartamentos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            apartamento apartamento = db.apartamentos.Find(id);
            if (apartamento == null)
            {
                return HttpNotFound();
            }
            return View(apartamento);
        }

        //
        // POST: /apartamentos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            apartamento apartamento = db.apartamentos.Find(id);
            db.apartamentos.Remove(apartamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}