﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class pagoadminsController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /pagoadmins/

        public ActionResult Index()
        {
            var pago_admin = db.pago_admin.Include(p => p.administrador);
            return View(pago_admin.ToList());
        }

        //
        // GET: /pagoadmins/Details/5

        public ActionResult Details(DateTime id)
        {
            pago_admin pago_admin = db.pago_admin.Find(id);
            if (pago_admin == null)
            {
                return HttpNotFound();
            }
            return View(pago_admin);
        }

        //
        // GET: /pagoadmins/Create

        public ActionResult Create()
        {
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1");
            return View();
        }

        //
        // POST: /pagoadmins/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(pago_admin pago_admin)
        {
            if (ModelState.IsValid)
            {
                db.pago_admin.Add(pago_admin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", pago_admin.Cedula_Admin);
            return View(pago_admin);
        }

        //
        // GET: /pagoadmins/Edit/5

        public ActionResult Edit(DateTime id)
        {
            pago_admin pago_admin = db.pago_admin.Find(id);
            if (pago_admin == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", pago_admin.Cedula_Admin);
            return View(pago_admin);
        }

        //
        // POST: /pagoadmins/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(pago_admin pago_admin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pago_admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", pago_admin.Cedula_Admin);
            return View(pago_admin);
        }

        //
        // GET: /pagoadmins/Delete/5

        public ActionResult Delete(DateTime id)
        {
            pago_admin pago_admin = db.pago_admin.Find(id);
            if (pago_admin == null)
            {
                return HttpNotFound();
            }
            return View(pago_admin);
        }

        //
        // POST: /pagoadmins/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(DateTime id)
        {
            pago_admin pago_admin = db.pago_admin.Find(id);
            db.pago_admin.Remove(pago_admin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}