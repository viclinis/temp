﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class vifiadoresController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /vifiadores/

        public ActionResult Index()
        {
            return View(db.vi_fiador.ToList());
        }

        //
        // GET: /vifiadores/Details/5

        public ActionResult Details(int id = 0)
        {
            vi_fiador vi_fiador = db.vi_fiador.Find(id);
            if (vi_fiador == null)
            {
                return HttpNotFound();
            }
            return View(vi_fiador);
        }

        //
        // GET: /vifiadores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /vifiadores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vi_fiador vi_fiador)
        {
            if (ModelState.IsValid)
            {
                db.vi_fiador.Add(vi_fiador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vi_fiador);
        }

        //
        // GET: /vifiadores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            vi_fiador vi_fiador = db.vi_fiador.Find(id);
            if (vi_fiador == null)
            {
                return HttpNotFound();
            }
            return View(vi_fiador);
        }

        //
        // POST: /vifiadores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vi_fiador vi_fiador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vi_fiador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vi_fiador);
        }

        //
        // GET: /vifiadores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            vi_fiador vi_fiador = db.vi_fiador.Find(id);
            if (vi_fiador == null)
            {
                return HttpNotFound();
            }
            return View(vi_fiador);
        }

        //
        // POST: /vifiadores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            vi_fiador vi_fiador = db.vi_fiador.Find(id);
            db.vi_fiador.Remove(vi_fiador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}