﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class aptosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /aptos/

        public ActionResult Index()
        {
            return View(db.aptos_ocu_deso.ToList());
        }

        //
        // GET: /aptos/Details/5

        public ActionResult Details(int id = 0)
        {
            aptos_ocu_deso aptos_ocu_deso = db.aptos_ocu_deso.Find(id);
            if (aptos_ocu_deso == null)
            {
                return HttpNotFound();
            }
            return View(aptos_ocu_deso);
        }

        //
        // GET: /aptos/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /aptos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(aptos_ocu_deso aptos_ocu_deso)
        {
            if (ModelState.IsValid)
            {
                db.aptos_ocu_deso.Add(aptos_ocu_deso);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aptos_ocu_deso);
        }

        //
        // GET: /aptos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            aptos_ocu_deso aptos_ocu_deso = db.aptos_ocu_deso.Find(id);
            if (aptos_ocu_deso == null)
            {
                return HttpNotFound();
            }
            return View(aptos_ocu_deso);
        }

        //
        // POST: /aptos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(aptos_ocu_deso aptos_ocu_deso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aptos_ocu_deso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aptos_ocu_deso);
        }

        //
        // GET: /aptos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            aptos_ocu_deso aptos_ocu_deso = db.aptos_ocu_deso.Find(id);
            if (aptos_ocu_deso == null)
            {
                return HttpNotFound();
            }
            return View(aptos_ocu_deso);
        }

        //
        // POST: /aptos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            aptos_ocu_deso aptos_ocu_deso = db.aptos_ocu_deso.Find(id);
            db.aptos_ocu_deso.Remove(aptos_ocu_deso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}