﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class auadministradoresController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /auadministradores/

        public ActionResult Index()
        {
            return View(db.audi_administrador.ToList());
        }

        //
        // GET: /auadministradores/Details/5

        public ActionResult Details(int id = 0)
        {
            audi_administrador audi_administrador = db.audi_administrador.Find(id);
            if (audi_administrador == null)
            {
                return HttpNotFound();
            }
            return View(audi_administrador);
        }

        //
        // GET: /auadministradores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /auadministradores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(audi_administrador audi_administrador)
        {
            if (ModelState.IsValid)
            {
                db.audi_administrador.Add(audi_administrador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(audi_administrador);
        }

        //
        // GET: /auadministradores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            audi_administrador audi_administrador = db.audi_administrador.Find(id);
            if (audi_administrador == null)
            {
                return HttpNotFound();
            }
            return View(audi_administrador);
        }

        //
        // POST: /auadministradores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(audi_administrador audi_administrador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(audi_administrador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(audi_administrador);
        }

        //
        // GET: /auadministradores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            audi_administrador audi_administrador = db.audi_administrador.Find(id);
            if (audi_administrador == null)
            {
                return HttpNotFound();
            }
            return View(audi_administrador);
        }

        //
        // POST: /auadministradores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            audi_administrador audi_administrador = db.audi_administrador.Find(id);
            db.audi_administrador.Remove(audi_administrador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}