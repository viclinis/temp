﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class estadocontratosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estadocontratos/

        public ActionResult Index()
        {
            return View(db.estado_contrato.ToList());
        }

        //
        // GET: /estadocontratos/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_contrato estado_contrato = db.estado_contrato.Find(id);
            if (estado_contrato == null)
            {
                return HttpNotFound();
            }
            return View(estado_contrato);
        }

        //
        // GET: /estadocontratos/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estadocontratos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_contrato estado_contrato)
        {
            if (ModelState.IsValid)
            {
                db.estado_contrato.Add(estado_contrato);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_contrato);
        }

        //
        // GET: /estadocontratos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_contrato estado_contrato = db.estado_contrato.Find(id);
            if (estado_contrato == null)
            {
                return HttpNotFound();
            }
            return View(estado_contrato);
        }

        //
        // POST: /estadocontratos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_contrato estado_contrato)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_contrato).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_contrato);
        }

        //
        // GET: /estadocontratos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_contrato estado_contrato = db.estado_contrato.Find(id);
            if (estado_contrato == null)
            {
                return HttpNotFound();
            }
            return View(estado_contrato);
        }

        //
        // POST: /estadocontratos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_contrato estado_contrato = db.estado_contrato.Find(id);
            db.estado_contrato.Remove(estado_contrato);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}