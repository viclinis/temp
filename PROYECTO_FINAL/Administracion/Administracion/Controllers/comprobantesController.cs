﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class comprobantesController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /comprobantes/

        public ActionResult Index()
        {
            var comprobantes_pago = db.comprobantes_pago.Include(c => c.apartamento).Include(c => c.estado_copa);
            return View(comprobantes_pago.ToList());
        }

        //
        // GET: /comprobantes/Details/5

        public ActionResult Details(int id = 0)
        {
            comprobantes_pago comprobantes_pago = db.comprobantes_pago.Find(id);
            if (comprobantes_pago == null)
            {
                return HttpNotFound();
            }
            return View(comprobantes_pago);
        }

        //
        // GET: /comprobantes/Create

        public ActionResult Create()
        {
            ViewBag.Fk_NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones");
            ViewBag.Id_EstadoCopa = new SelectList(db.estado_copa, "Id_EstadoCopa", "Id_EstadoCopa");
            return View();
        }

        //
        // POST: /comprobantes/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(comprobantes_pago comprobantes_pago)
        {
            if (ModelState.IsValid)
            {
                db.comprobantes_pago.Add(comprobantes_pago);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Fk_NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", comprobantes_pago.Fk_NoApto);
            ViewBag.Id_EstadoCopa = new SelectList(db.estado_copa, "Id_EstadoCopa", "Id_EstadoCopa", comprobantes_pago.Id_EstadoCopa);
            return View(comprobantes_pago);
        }

        //
        // GET: /comprobantes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            comprobantes_pago comprobantes_pago = db.comprobantes_pago.Find(id);
            if (comprobantes_pago == null)
            {
                return HttpNotFound();
            }
            ViewBag.Fk_NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", comprobantes_pago.Fk_NoApto);
            ViewBag.Id_EstadoCopa = new SelectList(db.estado_copa, "Id_EstadoCopa", "Id_EstadoCopa", comprobantes_pago.Id_EstadoCopa);
            return View(comprobantes_pago);
        }

        //
        // POST: /comprobantes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(comprobantes_pago comprobantes_pago)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comprobantes_pago).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Fk_NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", comprobantes_pago.Fk_NoApto);
            ViewBag.Id_EstadoCopa = new SelectList(db.estado_copa, "Id_EstadoCopa", "Id_EstadoCopa", comprobantes_pago.Id_EstadoCopa);
            return View(comprobantes_pago);
        }

        //
        // GET: /comprobantes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            comprobantes_pago comprobantes_pago = db.comprobantes_pago.Find(id);
            if (comprobantes_pago == null)
            {
                return HttpNotFound();
            }
            return View(comprobantes_pago);
        }

        //
        // POST: /comprobantes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            comprobantes_pago comprobantes_pago = db.comprobantes_pago.Find(id);
            db.comprobantes_pago.Remove(comprobantes_pago);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}