﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class estadoserpubsController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estadoserpubs/

        public ActionResult Index()
        {
            return View(db.estado_serpub.ToList());
        }

        //
        // GET: /estadoserpubs/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // GET: /estadoserpubs/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estadoserpubs/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_serpub estado_serpub)
        {
            if (ModelState.IsValid)
            {
                db.estado_serpub.Add(estado_serpub);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_serpub);
        }

        //
        // GET: /estadoserpubs/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // POST: /estadoserpubs/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_serpub estado_serpub)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_serpub).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_serpub);
        }

        //
        // GET: /estadoserpubs/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // POST: /estadoserpubs/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            db.estado_serpub.Remove(estado_serpub);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}