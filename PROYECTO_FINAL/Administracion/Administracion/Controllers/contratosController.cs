﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class contratosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /contratos/

        public ActionResult Index()
        {
            var contratoes = db.contratoes.Include(c => c.apartamento).Include(c => c.estado_contrato);
            return View(contratoes.ToList());
        }

        //
        // GET: /contratos/Details/5

        public ActionResult Details(int id = 0)
        {
            contrato contrato = db.contratoes.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            return View(contrato);
        }

        //
        // GET: /contratos/Create

        public ActionResult Create()
        {
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones");
            ViewBag.Id_EstadoContrato = new SelectList(db.estado_contrato, "Id_EstadoContrato", "Id_EstadoContrato");
            return View();
        }

        //
        // POST: /contratos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(contrato contrato)
        {
            if (ModelState.IsValid)
            {
                db.contratoes.Add(contrato);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", contrato.NoApto);
            ViewBag.Id_EstadoContrato = new SelectList(db.estado_contrato, "Id_EstadoContrato", "Id_EstadoContrato", contrato.Id_EstadoContrato);
            return View(contrato);
        }

        //
        // GET: /contratos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            contrato contrato = db.contratoes.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", contrato.NoApto);
            ViewBag.Id_EstadoContrato = new SelectList(db.estado_contrato, "Id_EstadoContrato", "Id_EstadoContrato", contrato.Id_EstadoContrato);
            return View(contrato);
        }

        //
        // POST: /contratos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(contrato contrato)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contrato).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", contrato.NoApto);
            ViewBag.Id_EstadoContrato = new SelectList(db.estado_contrato, "Id_EstadoContrato", "Id_EstadoContrato", contrato.Id_EstadoContrato);
            return View(contrato);
        }

        //
        // GET: /contratos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            contrato contrato = db.contratoes.Find(id);
            if (contrato == null)
            {
                return HttpNotFound();
            }
            return View(contrato);
        }

        //
        // POST: /contratos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            contrato contrato = db.contratoes.Find(id);
            db.contratoes.Remove(contrato);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}