﻿using Administracion.Controllers;
using Administracion.Filters;
using Administracion.Models;
using MySql;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.EntityModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Administracion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Apartamentos()
        {
            ViewBag.Message = "Apartamentos.";
            return View();
        }
    }
}
