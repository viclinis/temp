﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class arrendatariosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /arrendatarios/

        public ActionResult Index()
        {
            var arrendatarios = db.arrendatarios.Include(a => a.contrato).Include(a => a.tipo_arrenda);
            return View(arrendatarios.ToList());
        }

        //
        // GET: /arrendatarios/Details/5

        public ActionResult Details(int id = 0)
        {
            arrendatario arrendatario = db.arrendatarios.Find(id);
            if (arrendatario == null)
            {
                return HttpNotFound();
            }
            return View(arrendatario);
        }

        //
        // GET: /arrendatarios/Create

        public ActionResult Create()
        {
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato");
            ViewBag.Id_TipoArrenda = new SelectList(db.tipo_arrenda, "Id_TipoArrenda", "TipoArrenda");
            return View();
        }

        //
        // POST: /arrendatarios/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(arrendatario arrendatario)
        {
            if (ModelState.IsValid)
            {
                db.arrendatarios.Add(arrendatario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", arrendatario.Cod_contrato);
            ViewBag.Id_TipoArrenda = new SelectList(db.tipo_arrenda, "Id_TipoArrenda", "TipoArrenda", arrendatario.Id_TipoArrenda);
            return View(arrendatario);
        }

        //
        // GET: /arrendatarios/Edit/5

        public ActionResult Edit(int id = 0)
        {
            arrendatario arrendatario = db.arrendatarios.Find(id);
            if (arrendatario == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", arrendatario.Cod_contrato);
            ViewBag.Id_TipoArrenda = new SelectList(db.tipo_arrenda, "Id_TipoArrenda", "TipoArrenda", arrendatario.Id_TipoArrenda);
            return View(arrendatario);
        }

        //
        // POST: /arrendatarios/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(arrendatario arrendatario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(arrendatario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", arrendatario.Cod_contrato);
            ViewBag.Id_TipoArrenda = new SelectList(db.tipo_arrenda, "Id_TipoArrenda", "TipoArrenda", arrendatario.Id_TipoArrenda);
            return View(arrendatario);
        }

        //
        // GET: /arrendatarios/Delete/5

        public ActionResult Delete(int id = 0)
        {
            arrendatario arrendatario = db.arrendatarios.Find(id);
            if (arrendatario == null)
            {
                return HttpNotFound();
            }
            return View(arrendatario);
        }

        //
        // POST: /arrendatarios/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            arrendatario arrendatario = db.arrendatarios.Find(id);
            db.arrendatarios.Remove(arrendatario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}