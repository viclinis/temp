﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class propietariosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /propietarios/

        public ActionResult Index()
        {
            return View(db.propietario_edificio.ToList());
        }

        //
        // GET: /propietarios/Details/5

        public ActionResult Details(int id = 0)
        {
            propietario_edificio propietario_edificio = db.propietario_edificio.Find(id);
            if (propietario_edificio == null)
            {
                return HttpNotFound();
            }
            return View(propietario_edificio);
        }

        //
        // GET: /propietarios/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /propietarios/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(propietario_edificio propietario_edificio)
        {
            if (ModelState.IsValid)
            {
                db.propietario_edificio.Add(propietario_edificio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(propietario_edificio);
        }

        //
        // GET: /propietarios/Edit/5

        public ActionResult Edit(int id = 0)
        {
            propietario_edificio propietario_edificio = db.propietario_edificio.Find(id);
            if (propietario_edificio == null)
            {
                return HttpNotFound();
            }
            return View(propietario_edificio);
        }

        //
        // POST: /propietarios/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(propietario_edificio propietario_edificio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(propietario_edificio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(propietario_edificio);
        }

        //
        // GET: /propietarios/Delete/5

        public ActionResult Delete(int id = 0)
        {
            propietario_edificio propietario_edificio = db.propietario_edificio.Find(id);
            if (propietario_edificio == null)
            {
                return HttpNotFound();
            }
            return View(propietario_edificio);
        }

        //
        // POST: /propietarios/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            propietario_edificio propietario_edificio = db.propietario_edificio.Find(id);
            db.propietario_edificio.Remove(propietario_edificio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}