﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class estadofiadoresController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estadofiadores/

        public ActionResult Index()
        {
            return View(db.estado_fiador.ToList());
        }

        //
        // GET: /estadofiadores/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_fiador estado_fiador = db.estado_fiador.Find(id);
            if (estado_fiador == null)
            {
                return HttpNotFound();
            }
            return View(estado_fiador);
        }

        //
        // GET: /estadofiadores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estadofiadores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_fiador estado_fiador)
        {
            if (ModelState.IsValid)
            {
                db.estado_fiador.Add(estado_fiador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_fiador);
        }

        //
        // GET: /estadofiadores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_fiador estado_fiador = db.estado_fiador.Find(id);
            if (estado_fiador == null)
            {
                return HttpNotFound();
            }
            return View(estado_fiador);
        }

        //
        // POST: /estadofiadores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_fiador estado_fiador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_fiador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_fiador);
        }

        //
        // GET: /estadofiadores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_fiador estado_fiador = db.estado_fiador.Find(id);
            if (estado_fiador == null)
            {
                return HttpNotFound();
            }
            return View(estado_fiador);
        }

        //
        // POST: /estadofiadores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_fiador estado_fiador = db.estado_fiador.Find(id);
            db.estado_fiador.Remove(estado_fiador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}