﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class observacionesController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /observaciones/

        public ActionResult Index()
        {
            var observaciones = db.observaciones.Include(o => o.apartamento).Include(o => o.estado_obser).Include(o => o.tipo_observacion);
            return View(observaciones.ToList());
        }

        //
        // GET: /observaciones/Details/5

        public ActionResult Details(int id = 0)
        {
            observacione observacione = db.observaciones.Find(id);
            if (observacione == null)
            {
                return HttpNotFound();
            }
            return View(observacione);
        }

        //
        // GET: /observaciones/Create

        public ActionResult Create()
        {
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones");
            ViewBag.Id_EstadoObser = new SelectList(db.estado_obser, "Id_EstadoObser", "Id_EstadoObser");
            ViewBag.Id_TipoObser = new SelectList(db.tipo_observacion, "Id_TipoObser", "TipoObser");
            return View();
        }

        //
        // POST: /observaciones/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(observacione observacione)
        {
            if (ModelState.IsValid)
            {
                db.observaciones.Add(observacione);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", observacione.NoApto);
            ViewBag.Id_EstadoObser = new SelectList(db.estado_obser, "Id_EstadoObser", "Id_EstadoObser", observacione.Id_EstadoObser);
            ViewBag.Id_TipoObser = new SelectList(db.tipo_observacion, "Id_TipoObser", "TipoObser", observacione.Id_TipoObser);
            return View(observacione);
        }

        //
        // GET: /observaciones/Edit/5

        public ActionResult Edit(int id = 0)
        {
            observacione observacione = db.observaciones.Find(id);
            if (observacione == null)
            {
                return HttpNotFound();
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", observacione.NoApto);
            ViewBag.Id_EstadoObser = new SelectList(db.estado_obser, "Id_EstadoObser", "Id_EstadoObser", observacione.Id_EstadoObser);
            ViewBag.Id_TipoObser = new SelectList(db.tipo_observacion, "Id_TipoObser", "TipoObser", observacione.Id_TipoObser);
            return View(observacione);
        }

        //
        // POST: /observaciones/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(observacione observacione)
        {
            if (ModelState.IsValid)
            {
                db.Entry(observacione).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", observacione.NoApto);
            ViewBag.Id_EstadoObser = new SelectList(db.estado_obser, "Id_EstadoObser", "Id_EstadoObser", observacione.Id_EstadoObser);
            ViewBag.Id_TipoObser = new SelectList(db.tipo_observacion, "Id_TipoObser", "TipoObser", observacione.Id_TipoObser);
            return View(observacione);
        }

        //
        // GET: /observaciones/Delete/5

        public ActionResult Delete(int id = 0)
        {
            observacione observacione = db.observaciones.Find(id);
            if (observacione == null)
            {
                return HttpNotFound();
            }
            return View(observacione);
        }

        //
        // POST: /observaciones/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            observacione observacione = db.observaciones.Find(id);
            db.observaciones.Remove(observacione);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}