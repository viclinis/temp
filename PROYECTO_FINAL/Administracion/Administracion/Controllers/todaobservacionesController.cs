﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Administracion.Models;

namespace Administracion.Controllers
{
    public class todaobservacionesController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /todaobservaciones/

        public ActionResult Index()
        {
            return View(db.toda_observacion.ToList());
        }

        //
        // GET: /todaobservaciones/Details/5

        public ActionResult Details(int id = 0)
        {
            toda_observacion toda_observacion = db.toda_observacion.Find(id);
            if (toda_observacion == null)
            {
                return HttpNotFound();
            }
            return View(toda_observacion);
        }

        //
        // GET: /todaobservaciones/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /todaobservaciones/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(toda_observacion toda_observacion)
        {
            if (ModelState.IsValid)
            {
                db.toda_observacion.Add(toda_observacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(toda_observacion);
        }

        //
        // GET: /todaobservaciones/Edit/5

        public ActionResult Edit(int id = 0)
        {
            toda_observacion toda_observacion = db.toda_observacion.Find(id);
            if (toda_observacion == null)
            {
                return HttpNotFound();
            }
            return View(toda_observacion);
        }

        //
        // POST: /todaobservaciones/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(toda_observacion toda_observacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toda_observacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(toda_observacion);
        }

        //
        // GET: /todaobservaciones/Delete/5

        public ActionResult Delete(int id = 0)
        {
            toda_observacion toda_observacion = db.toda_observacion.Find(id);
            if (toda_observacion == null)
            {
                return HttpNotFound();
            }
            return View(toda_observacion);
        }

        //
        // POST: /todaobservaciones/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            toda_observacion toda_observacion = db.toda_observacion.Find(id);
            db.toda_observacion.Remove(toda_observacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}