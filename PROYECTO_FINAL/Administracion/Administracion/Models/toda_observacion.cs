//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Administracion.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class toda_observacion
    {
        public string Detalle { get; set; }
        public int Apartamento { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }
    }
}
