//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inmobiliaria_MyM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class administrador
    {
        public administrador()
        {
            this.edificios = new HashSet<edificio>();
            this.pago_admin = new HashSet<pago_admin>();
        }
    
        public int Cedula_Admin { get; set; }
        public string Nombre1 { get; set; }
        public string Nombre2 { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public Nullable<int> NoCelular { get; set; }
    
        public virtual ICollection<edificio> edificios { get; set; }
        public virtual ICollection<pago_admin> pago_admin { get; set; }
    }
}
