//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inmobiliaria_MyM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class aptos_servicios
    {
        public int Cod_SerPublic { get; set; }
        public int NoApto { get; set; }
        public Nullable<System.DateTime> Fecha_Install { get; set; }
    
        public virtual apartamento apartamento { get; set; }
        public virtual servicios_publicos servicios_publicos { get; set; }
    }
}
