﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class pisosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /pisos/

        public ActionResult Index()
        {
            var pisoes = db.pisoes.Include(p => p.edificio);
            return View(pisoes.ToList());
        }

        //
        // GET: /pisos/Details/5

        public ActionResult Details(string id = null)
        {
            piso piso = db.pisoes.Find(id);
            if (piso == null)
            {
                return HttpNotFound();
            }
            return View(piso);
        }

        //
        // GET: /pisos/Create

        public ActionResult Create()
        {
            ViewBag.Nit = new SelectList(db.edificios, "Nit", "Nombre");
            return View();
        }

        //
        // POST: /pisos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(piso piso)
        {
            if (ModelState.IsValid)
            {
                db.pisoes.Add(piso);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Nit = new SelectList(db.edificios, "Nit", "Nombre", piso.Nit);
            return View(piso);
        }

        //
        // GET: /pisos/Edit/5

        public ActionResult Edit(string id = null)
        {
            piso piso = db.pisoes.Find(id);
            if (piso == null)
            {
                return HttpNotFound();
            }
            ViewBag.Nit = new SelectList(db.edificios, "Nit", "Nombre", piso.Nit);
            return View(piso);
        }

        //
        // POST: /pisos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(piso piso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(piso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Nit = new SelectList(db.edificios, "Nit", "Nombre", piso.Nit);
            return View(piso);
        }

        //
        // GET: /pisos/Delete/5

        public ActionResult Delete(string id = null)
        {
            piso piso = db.pisoes.Find(id);
            if (piso == null)
            {
                return HttpNotFound();
            }
            return View(piso);
        }

        //
        // POST: /pisos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            piso piso = db.pisoes.Find(id);
            db.pisoes.Remove(piso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}