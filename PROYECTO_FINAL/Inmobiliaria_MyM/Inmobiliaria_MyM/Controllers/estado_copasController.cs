﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class estado_copasController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estado_copas/

        public ActionResult Index()
        {
            return View(db.estado_copa.ToList());
        }

        //
        // GET: /estado_copas/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // GET: /estado_copas/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estado_copas/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_copa estado_copa)
        {
            if (ModelState.IsValid)
            {
                db.estado_copa.Add(estado_copa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_copa);
        }

        //
        // GET: /estado_copas/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // POST: /estado_copas/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_copa estado_copa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_copa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_copa);
        }

        //
        // GET: /estado_copas/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            if (estado_copa == null)
            {
                return HttpNotFound();
            }
            return View(estado_copa);
        }

        //
        // POST: /estado_copas/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_copa estado_copa = db.estado_copa.Find(id);
            db.estado_copa.Remove(estado_copa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}