﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class estado_obsersController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estado_obsers/

        public ActionResult Index()
        {
            return View(db.estado_obser.ToList());
        }

        //
        // GET: /estado_obsers/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // GET: /estado_obsers/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estado_obsers/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_obser estado_obser)
        {
            if (ModelState.IsValid)
            {
                db.estado_obser.Add(estado_obser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_obser);
        }

        //
        // GET: /estado_obsers/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // POST: /estado_obsers/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_obser estado_obser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_obser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_obser);
        }

        //
        // GET: /estado_obsers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            if (estado_obser == null)
            {
                return HttpNotFound();
            }
            return View(estado_obser);
        }

        //
        // POST: /estado_obsers/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_obser estado_obser = db.estado_obser.Find(id);
            db.estado_obser.Remove(estado_obser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}