﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class fiadoresController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /fiadores/

        public ActionResult Index()
        {
            var fiadors = db.fiadors.Include(f => f.contrato).Include(f => f.estado_fiador);
            return View(fiadors.ToList());
        }

        //
        // GET: /fiadores/Details/5

        public ActionResult Details(int id = 0)
        {
            fiador fiador = db.fiadors.Find(id);
            if (fiador == null)
            {
                return HttpNotFound();
            }
            return View(fiador);
        }

        //
        // GET: /fiadores/Create

        public ActionResult Create()
        {
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato");
            ViewBag.Id_EstadoFiador = new SelectList(db.estado_fiador, "Id_EstadoFiador", "Id_EstadoFiador");
            return View();
        }

        //
        // POST: /fiadores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(fiador fiador)
        {
            if (ModelState.IsValid)
            {
                db.fiadors.Add(fiador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", fiador.Cod_contrato);
            ViewBag.Id_EstadoFiador = new SelectList(db.estado_fiador, "Id_EstadoFiador", "Id_EstadoFiador", fiador.Id_EstadoFiador);
            return View(fiador);
        }

        //
        // GET: /fiadores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            fiador fiador = db.fiadors.Find(id);
            if (fiador == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", fiador.Cod_contrato);
            ViewBag.Id_EstadoFiador = new SelectList(db.estado_fiador, "Id_EstadoFiador", "Id_EstadoFiador", fiador.Id_EstadoFiador);
            return View(fiador);
        }

        //
        // POST: /fiadores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(fiador fiador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fiador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cod_contrato = new SelectList(db.contratoes, "Cod_contrato", "Cod_contrato", fiador.Cod_contrato);
            ViewBag.Id_EstadoFiador = new SelectList(db.estado_fiador, "Id_EstadoFiador", "Id_EstadoFiador", fiador.Id_EstadoFiador);
            return View(fiador);
        }

        //
        // GET: /fiadores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            fiador fiador = db.fiadors.Find(id);
            if (fiador == null)
            {
                return HttpNotFound();
            }
            return View(fiador);
        }

        //
        // POST: /fiadores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            fiador fiador = db.fiadors.Find(id);
            db.fiadors.Remove(fiador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}