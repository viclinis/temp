﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class tipo_arrendasController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /tipo_arrendas/

        public ActionResult Index()
        {
            return View(db.tipo_arrenda.ToList());
        }

        //
        // GET: /tipo_arrendas/Details/5

        public ActionResult Details(int id = 0)
        {
            tipo_arrenda tipo_arrenda = db.tipo_arrenda.Find(id);
            if (tipo_arrenda == null)
            {
                return HttpNotFound();
            }
            return View(tipo_arrenda);
        }

        //
        // GET: /tipo_arrendas/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /tipo_arrendas/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tipo_arrenda tipo_arrenda)
        {
            if (ModelState.IsValid)
            {
                db.tipo_arrenda.Add(tipo_arrenda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipo_arrenda);
        }

        //
        // GET: /tipo_arrendas/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tipo_arrenda tipo_arrenda = db.tipo_arrenda.Find(id);
            if (tipo_arrenda == null)
            {
                return HttpNotFound();
            }
            return View(tipo_arrenda);
        }

        //
        // POST: /tipo_arrendas/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tipo_arrenda tipo_arrenda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_arrenda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipo_arrenda);
        }

        //
        // GET: /tipo_arrendas/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tipo_arrenda tipo_arrenda = db.tipo_arrenda.Find(id);
            if (tipo_arrenda == null)
            {
                return HttpNotFound();
            }
            return View(tipo_arrenda);
        }

        //
        // POST: /tipo_arrendas/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_arrenda tipo_arrenda = db.tipo_arrenda.Find(id);
            db.tipo_arrenda.Remove(tipo_arrenda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}