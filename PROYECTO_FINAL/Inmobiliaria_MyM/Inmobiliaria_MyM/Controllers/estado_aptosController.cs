﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class estado_aptosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estado_aptos/

        public ActionResult Index()
        {
            return View(db.estado_apto.ToList());
        }

        //
        // GET: /estado_aptos/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_apto estado_apto = db.estado_apto.Find(id);
            if (estado_apto == null)
            {
                return HttpNotFound();
            }
            return View(estado_apto);
        }

        //
        // GET: /estado_aptos/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estado_aptos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_apto estado_apto)
        {
            if (ModelState.IsValid)
            {
                db.estado_apto.Add(estado_apto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_apto);
        }

        //
        // GET: /estado_aptos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_apto estado_apto = db.estado_apto.Find(id);
            if (estado_apto == null)
            {
                return HttpNotFound();
            }
            return View(estado_apto);
        }

        //
        // POST: /estado_aptos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_apto estado_apto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_apto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_apto);
        }

        //
        // GET: /estado_aptos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_apto estado_apto = db.estado_apto.Find(id);
            if (estado_apto == null)
            {
                return HttpNotFound();
            }
            return View(estado_apto);
        }

        //
        // POST: /estado_aptos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_apto estado_apto = db.estado_apto.Find(id);
            db.estado_apto.Remove(estado_apto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}