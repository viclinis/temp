﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class administradoresController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /administradores/

        public ActionResult Index()
        {
            return View(db.administradors.ToList());
        }

        //
        // GET: /administradores/Details/5

        public ActionResult Details(int id = 0)
        {
            administrador administrador = db.administradors.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        //
        // GET: /administradores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /administradores/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(administrador administrador)
        {
            if (ModelState.IsValid)
            {
                db.administradors.Add(administrador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(administrador);
        }

        //
        // GET: /administradores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            administrador administrador = db.administradors.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        //
        // POST: /administradores/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(administrador administrador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(administrador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(administrador);
        }

        //
        // GET: /administradores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            administrador administrador = db.administradors.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        //
        // POST: /administradores/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            administrador administrador = db.administradors.Find(id);
            db.administradors.Remove(administrador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}