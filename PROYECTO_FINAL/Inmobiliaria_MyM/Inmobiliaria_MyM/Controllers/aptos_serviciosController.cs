﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class aptos_serviciosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /aptos_servicios/

        public ActionResult Index()
        {
            var aptos_servicios = db.aptos_servicios.Include(a => a.apartamento).Include(a => a.servicios_publicos);
            return View(aptos_servicios.ToList());
        }

        //
        // GET: /aptos_servicios/Details/5

        public ActionResult Details(int id = 0)
        {
            aptos_servicios aptos_servicios = db.aptos_servicios.Find(id);
            if (aptos_servicios == null)
            {
                return HttpNotFound();
            }
            return View(aptos_servicios);
        }

        //
        // GET: /aptos_servicios/Create

        public ActionResult Create()
        {
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones");
            ViewBag.Cod_SerPublic = new SelectList(db.servicios_publicos, "Cod_SerPublic", "Nombre_servi");
            return View();
        }

        //
        // POST: /aptos_servicios/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(aptos_servicios aptos_servicios)
        {
            if (ModelState.IsValid)
            {
                db.aptos_servicios.Add(aptos_servicios);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", aptos_servicios.NoApto);
            ViewBag.Cod_SerPublic = new SelectList(db.servicios_publicos, "Cod_SerPublic", "Nombre_servi", aptos_servicios.Cod_SerPublic);
            return View(aptos_servicios);
        }

        //
        // GET: /aptos_servicios/Edit/5

        public ActionResult Edit(int id = 0)
        {
            aptos_servicios aptos_servicios = db.aptos_servicios.Find(id);
            if (aptos_servicios == null)
            {
                return HttpNotFound();
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", aptos_servicios.NoApto);
            ViewBag.Cod_SerPublic = new SelectList(db.servicios_publicos, "Cod_SerPublic", "Nombre_servi", aptos_servicios.Cod_SerPublic);
            return View(aptos_servicios);
        }

        //
        // POST: /aptos_servicios/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(aptos_servicios aptos_servicios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aptos_servicios).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NoApto = new SelectList(db.apartamentos, "NoApto", "Habitaciones", aptos_servicios.NoApto);
            ViewBag.Cod_SerPublic = new SelectList(db.servicios_publicos, "Cod_SerPublic", "Nombre_servi", aptos_servicios.Cod_SerPublic);
            return View(aptos_servicios);
        }

        //
        // GET: /aptos_servicios/Delete/5

        public ActionResult Delete(int id = 0)
        {
            aptos_servicios aptos_servicios = db.aptos_servicios.Find(id);
            if (aptos_servicios == null)
            {
                return HttpNotFound();
            }
            return View(aptos_servicios);
        }

        //
        // POST: /aptos_servicios/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            aptos_servicios aptos_servicios = db.aptos_servicios.Find(id);
            db.aptos_servicios.Remove(aptos_servicios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}