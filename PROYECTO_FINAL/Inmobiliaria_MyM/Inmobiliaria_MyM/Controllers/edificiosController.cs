﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class edificiosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /edificios/

        public ActionResult Index()
        {
            var edificios = db.edificios.Include(e => e.administrador).Include(e => e.propietario_edificio);
            return View(edificios.ToList());
        }

        //
        // GET: /edificios/Details/5

        public ActionResult Details(int id = 0)
        {
            edificio edificio = db.edificios.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            return View(edificio);
        }

        //
        // GET: /edificios/Create

        public ActionResult Create()
        {
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1");
            ViewBag.Cedula_Propie = new SelectList(db.propietario_edificio, "C_Cedula_Propie", "Nombre");
            return View();
        }

        //
        // POST: /edificios/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(edificio edificio)
        {
            if (ModelState.IsValid)
            {
                db.edificios.Add(edificio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", edificio.Cedula_Admin);
            ViewBag.Cedula_Propie = new SelectList(db.propietario_edificio, "C_Cedula_Propie", "Nombre", edificio.Cedula_Propie);
            return View(edificio);
        }

        //
        // GET: /edificios/Edit/5

        public ActionResult Edit(int id = 0)
        {
            edificio edificio = db.edificios.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", edificio.Cedula_Admin);
            ViewBag.Cedula_Propie = new SelectList(db.propietario_edificio, "C_Cedula_Propie", "Nombre", edificio.Cedula_Propie);
            return View(edificio);
        }

        //
        // POST: /edificios/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(edificio edificio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(edificio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cedula_Admin = new SelectList(db.administradors, "Cedula_Admin", "Nombre1", edificio.Cedula_Admin);
            ViewBag.Cedula_Propie = new SelectList(db.propietario_edificio, "C_Cedula_Propie", "Nombre", edificio.Cedula_Propie);
            return View(edificio);
        }

        //
        // GET: /edificios/Delete/5

        public ActionResult Delete(int id = 0)
        {
            edificio edificio = db.edificios.Find(id);
            if (edificio == null)
            {
                return HttpNotFound();
            }
            return View(edificio);
        }

        //
        // POST: /edificios/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            edificio edificio = db.edificios.Find(id);
            db.edificios.Remove(edificio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}