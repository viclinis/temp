﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class servicios_publicosController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /servicios_publicos/

        public ActionResult Index()
        {
            var servicios_publicos = db.servicios_publicos.Include(s => s.estado_serpub);
            return View(servicios_publicos.ToList());
        }

        //
        // GET: /servicios_publicos/Details/5

        public ActionResult Details(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            return View(servicios_publicos);
        }

        //
        // GET: /servicios_publicos/Create

        public ActionResult Create()
        {
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub");
            return View();
        }

        //
        // POST: /servicios_publicos/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(servicios_publicos servicios_publicos)
        {
            if (ModelState.IsValid)
            {
                db.servicios_publicos.Add(servicios_publicos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // GET: /servicios_publicos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // POST: /servicios_publicos/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(servicios_publicos servicios_publicos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicios_publicos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_EstadoSerPub = new SelectList(db.estado_serpub, "Id_EstadoSerPub", "Id_EstadoSerPub", servicios_publicos.Id_EstadoSerPub);
            return View(servicios_publicos);
        }

        //
        // GET: /servicios_publicos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            if (servicios_publicos == null)
            {
                return HttpNotFound();
            }
            return View(servicios_publicos);
        }

        //
        // POST: /servicios_publicos/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            servicios_publicos servicios_publicos = db.servicios_publicos.Find(id);
            db.servicios_publicos.Remove(servicios_publicos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}