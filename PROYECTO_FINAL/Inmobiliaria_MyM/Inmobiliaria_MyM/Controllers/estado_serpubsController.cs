﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class estado_serpubsController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /estado_serpubs/

        public ActionResult Index()
        {
            return View(db.estado_serpub.ToList());
        }

        //
        // GET: /estado_serpubs/Details/5

        public ActionResult Details(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // GET: /estado_serpubs/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /estado_serpubs/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(estado_serpub estado_serpub)
        {
            if (ModelState.IsValid)
            {
                db.estado_serpub.Add(estado_serpub);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estado_serpub);
        }

        //
        // GET: /estado_serpubs/Edit/5

        public ActionResult Edit(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // POST: /estado_serpubs/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(estado_serpub estado_serpub)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estado_serpub).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estado_serpub);
        }

        //
        // GET: /estado_serpubs/Delete/5

        public ActionResult Delete(int id = 0)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            if (estado_serpub == null)
            {
                return HttpNotFound();
            }
            return View(estado_serpub);
        }

        //
        // POST: /estado_serpubs/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            estado_serpub estado_serpub = db.estado_serpub.Find(id);
            db.estado_serpub.Remove(estado_serpub);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}