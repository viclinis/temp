﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inmobiliaria_MyM.Models;

namespace Inmobiliaria_MyM.Controllers
{
    public class tipo_observacionesController : Controller
    {
        private administracionEntities db = new administracionEntities();

        //
        // GET: /tipo_observaciones/

        public ActionResult Index()
        {
            return View(db.tipo_observacion.ToList());
        }

        //
        // GET: /tipo_observaciones/Details/5

        public ActionResult Details(int id = 0)
        {
            tipo_observacion tipo_observacion = db.tipo_observacion.Find(id);
            if (tipo_observacion == null)
            {
                return HttpNotFound();
            }
            return View(tipo_observacion);
        }

        //
        // GET: /tipo_observaciones/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /tipo_observaciones/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tipo_observacion tipo_observacion)
        {
            if (ModelState.IsValid)
            {
                db.tipo_observacion.Add(tipo_observacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipo_observacion);
        }

        //
        // GET: /tipo_observaciones/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tipo_observacion tipo_observacion = db.tipo_observacion.Find(id);
            if (tipo_observacion == null)
            {
                return HttpNotFound();
            }
            return View(tipo_observacion);
        }

        //
        // POST: /tipo_observaciones/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tipo_observacion tipo_observacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_observacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipo_observacion);
        }

        //
        // GET: /tipo_observaciones/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tipo_observacion tipo_observacion = db.tipo_observacion.Find(id);
            if (tipo_observacion == null)
            {
                return HttpNotFound();
            }
            return View(tipo_observacion);
        }

        //
        // POST: /tipo_observaciones/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_observacion tipo_observacion = db.tipo_observacion.Find(id);
            db.tipo_observacion.Remove(tipo_observacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}